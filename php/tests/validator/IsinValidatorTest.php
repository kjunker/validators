<?php
declare(strict_types = 1);

namespace validator;

use InvalidArgumentException;
use model\Isin;
use PHPUnit\Framework\TestCase;
use util\exception\PregException;

/**
 * Class IsinValidatorTest
 * @package validator
 * @coversDefaultClass \validator\IsinValidator
 */
class IsinValidatorTest extends TestCase
{
    public function getInvalidIsinString(): array
    {
        return [
            [
                '',
                InvalidArgumentException::class
            ],
            [
                '234ABC',
                InvalidArgumentException::class
            ],
            [
                'XA2edre234W2',
                InvalidArgumentException::class
            ],
            [
                'ID234BY00127',
            ],
            [
                'FR012560389',
            ],
            [
                'DE000A2TR5B',
            ],
            [
                'CH047533914',
            ],
        ];
    }

    public function getValidIsin(): array
    {
        return [
            'France' => [
                [
                    'FR0125603896', // BNP Paribas SA
                    'FR0125614950', // Agence Centrale de Organismes de Securite Sociale
                ],
            ],
            'Germany' => [
                [
                    'DE000A2TR5B8', // Sparkasse Neunkirchen (AT20241)
                    'DE000A2TR570', // Landessparkasse zu Oldenburg
                    'DE000A2TR8G1', // Sparkasse Ulm
                ],
            ],
            'Switzerland' => [
                [
                    'CH0475324973', // Raiffeisen Switzerland B.V.
                    'CH0475339146', // Leonteq Securities AG
                ],
            ],
        ];
    }

    /**
     * @covers ::check
     * @dataProvider getValidIsin
     * @param array $isins
     */
    public function testCheckWithString(array $isins): void
    {
        foreach ($isins as $isin) {
            $this->assertTrue(IsinValidator::check($isin));
        }
    }

    /**
     * @covers ::check
     * @dataProvider getInvalidIsinString
     * @param string $isin
     */
    public function testCheckWithStringFalse(string $isin): void
    {
        $this->assertFalse(IsinValidator::check($isin));
    }

    /**
     * @covers ::check
     * @param array $isins
     * @throws PregException
     * @dataProvider getValidIsin
     */
    public function testCheckWithIsinObject(array $isins): void
    {
        foreach ($isins as $isin) {
            $this->assertTrue(IsinValidator::check(Isin::createFromString($isin)));
        }
    }

    /**
     * @covers ::check
     * @param string $isin
     * @param string|null $exceptionClass
     * @throws PregException
     * @dataProvider getInvalidIsinString
     */
    public function testCheckWithIsinObjectFails(
        string $isin,
        ?string $exceptionClass = null
    ): void {
        if (null !== $exceptionClass) {
            $this->expectException($exceptionClass);
        }
        $this->assertFalse(IsinValidator::check(Isin::createFromString($isin)));
    }

    /**
     * @covers ::validate
     * @dataProvider getValidIsin
     * @param array $isins
     */
    public function testValidateWithString(array $isins): void
    {
        foreach ($isins as $isin) {
            $this->assertIsString($isin, 'Expected string, got ' . gettype($isin));
            $validationResult = IsinValidator::validate($isin);
            $this->assertInstanceOf(ValidationResult::class, $validationResult);
            $this->assertTrue($validationResult->isValid());
            $this->assertNull($validationResult->getMessage());
            $this->assertNull($validationResult->getErrorCode());
            $this->assertNull($validationResult->getException());
        }
    }

    /**
     * @covers ::validate
     * @dataProvider getInvalidIsinString
     * @param string $isin
     * @param string|null $exceptionClass
     */
    public function testValidateWithStringInvalidResult(
        string $isin,
        ?string $exceptionClass = null
    ): void {
        $this->assertIsString($isin, 'Expected string, got ' . gettype($isin));
        $validationResult = IsinValidator::validate($isin);
        $this->assertInstanceOf(ValidationResult::class, $validationResult);
        $this->assertFalse($validationResult->isValid());
        $this->assertNotEmpty($validationResult->getMessage());
        $this->assertIsInt($validationResult->getErrorCode());
        if (null !== $exceptionClass) {
            $this->assertNotEmpty($validationResult->getException());
        }
    }

    /**
     * @covers ::validate
     * @dataProvider getValidIsin
     * @param array $isins
     * @throws PregException
     */
    public function testValidateWithIsinObject(array $isins): void
    {
        foreach ($isins as $isin) {
            $isin = Isin::createFromString($isin);
            $this->assertInstanceOf(
                Isin::class,
                $isin,
                'Expected Isin Object, got ' . gettype($isin)
            );
            $validationResult = IsinValidator::validate($isin);
            $this->assertInstanceOf(ValidationResult::class, $validationResult);
            $this->assertTrue($validationResult->isValid());
            $this->assertNull($validationResult->getMessage());
            $this->assertNull($validationResult->getErrorCode());
            $this->assertNull($validationResult->getException());
        }
    }

    /**
     * @covers ::validate
     * @dataProvider getInvalidIsinString
     * @param string $isin
     * @param string|null $exceptionClass
     * @throws PregException
     */
    public function testValidateWithIsinObjectFail(
        string $isin,
        ?string $exceptionClass = null
    ): void {
        if (null !== $exceptionClass) {
            $this->expectException($exceptionClass);
        }
        $isin = Isin::createFromString($isin);
        $this->assertInstanceOf(
            Isin::class,
            $isin,
            'Expected Isin Object, got ' . gettype($isin)
        );
        $validationResult = IsinValidator::validate($isin);
        $this->assertInstanceOf(ValidationResult::class, $validationResult);
        $this->assertFalse($validationResult->isValid());
        $this->assertNotNull($validationResult->getMessage());
        $this->assertIsString($validationResult->getMessage());
        $this->assertNotNull($validationResult->getErrorCode());
        $this->assertIsInt($validationResult->getErrorCode());
    }
}
