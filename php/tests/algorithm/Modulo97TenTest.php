<?php
declare(strict_types = 1);

namespace algorithm;

use PHPUnit\Framework\TestCase;

/**
 * Class Modulo97TenTest
 * @coversDefaultClass \algorithm\Modulo97Ten
 * @package algorithm
 */
class Modulo97TenTest extends TestCase
{

    public function getModulus97Data()
    {
        return [
            [
                'numeric' => '9876543210',
                'checkDigit' => '71',
            ],
            [
                'numeric' => '98765432109876543210',
                'checkDigit' => '09',
            ],
            [
                'numeric' => '987654321098765432109876543210',
                'checkDigit' => '75',
            ],
            [
                'numeric' => '9876543210987654321098765432109876543210',
                'checkDigit' => '11',
            ],
            [
                'numeric' => '98765432109876543210987654321098765432109876543210',
                'checkDigit' => '76',
            ],
            [
                'numeric' => '987654321098765432109876543210987654321098765432109876543210',
                'checkDigit' => '60',
            ],
        ];
    }

    /**
     * @covers ::getDigit
     * @testdox Tests that the Algorithm calculates the correct checkDigit
     * @dataProvider getModulus97Data
     * @param string $numeric
     * @param string $digit
     */
    public function testGetDigit(string $numeric, string $digit): void
    {
        $this->assertSame($digit, Modulo97Ten::getDigit($numeric));
    }

    /**
     * @covers ::check
     * @testdox Tests that the given Numeric strings are valid as expected
     * @dataProvider getModulus97Data
     * @param string $numeric
     * @param string $digit
     */
    public function testCheck(string $numeric, string $digit): void
    {
        $this->assertTrue(Modulo97Ten::check($numeric . $digit));
    }

    /**
     * @covers ::getDigit
     * @covers ::check
     * @testdox Tests the Creation of Check Digit and the Validation
     * @dataProvider getModulus97Data
     * @param string $numeric
     */
    public function testInversion(string $numeric): void
    {
        $this->assertTrue(
            Modulo97Ten::check(
                $numeric . Modulo97Ten::getDigit($numeric)
            )
        );
    }
}
