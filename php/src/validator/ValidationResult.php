<?php
declare(strict_types = 1);

namespace validator;

use Throwable;

class ValidationResult
{
    /**
     * @var string
     */
    private $classname;
    /**
     * @var int|null
     */
    private $errorCode;
    /**
     * @var \Throwable|null
     */
    private $exception;
    /**
     * @var string|null
     */
    private $message;

    public function __construct(
        string $classname,
        ?int $errorCode = null,
        ?string $message = null,
        ?Throwable $exception = null
    ) {
        $this->classname = $classname;
        $this->errorCode = $errorCode;
        $this->message = $message;
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getClassname(): string
    {
        return $this->classname;
    }

    /**
     * @return int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @return \Throwable|null
     */
    public function getException(): ?\Throwable
    {
        return $this->exception;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function isValid(): bool
    {
        return null === $this->getErrorCode()
            && null === $this->getException();
    }
}