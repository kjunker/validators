<?php
declare(strict_types = 1);

namespace validator;

use algorithm\Modulo10;
use InvalidArgumentException;
use model\Isin;
use util\Converter;
use util\exception\PregException;
use util\IsinVerifier;

/**
 * Class IsinValidator - Validates and/or checks a ISIN
 * @package validator
 */
class IsinValidator
{
    /**
     * @param string|Isin $isin
     * @return ValidationResult
     */
    public static function validate($isin): ValidationResult
    {
        try {
            IsinVerifier::verifyNonEmptyStringOrIsin($isin);
            if (is_string($isin)) {
                $isin = Isin::createFromString($isin);
            }
            if (null === $isin->getCheckdigit()) {
                return new ValidationResult(
                    __CLASS__,
                    9100,
                    'ISIN does not contains a checkdigit.'
                );
            }

            if (!self::check($isin)) {
                return new ValidationResult(
                    __CLASS__,
                    9000,
                    'ISIN is not valid.'
                );
            }

            return new ValidationResult(__CLASS__);
        } catch (InvalidArgumentException | PregException $exception) {
            return new ValidationResult(
                __CLASS__,
                $exception->getCode(),
                $exception->getMessage(),
                $exception
            );
        }
    }

    /**
     * @param string|Isin $isin
     * @return bool
     */
    public static function check($isin): bool
    {
        try {
            IsinVerifier::verifyNonEmptyStringOrIsin($isin);
            if (is_string($isin)) {
                $isin = Isin::createFromString($isin);
            }
            if (null === $isin->getCheckdigit()) {
                return false;
            }

            return Modulo10::check(
                Converter::convertToNumeric(
                    $isin->getIsin()
                )
            );
        } catch (InvalidArgumentException | PregException $argumentException) {
            return false;
        }
    }

}
