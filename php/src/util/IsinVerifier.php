<?php
declare(strict_types = 1);

namespace util;

use InvalidArgumentException;
use model\Isin;
use util\exception\PregException;

class IsinVerifier extends Verifier
{

    /**
     * Checks that the Input is a non empty String or an Isin Object
     * Throws an InvalidArgumentException otherwise
     * @param mixed $input
     * @throws InvalidArgumentException
     */
    public static function verifyNonEmptyStringOrIsin($input): void
    {
        if ($input instanceof Isin || (is_string($input) && '' !== $input)) {
            return;
        }

        throw new InvalidArgumentException('Input must be an non empty string or Isin Object');
    }

    /**
     * @param $input
     * @throws PregException
     */
    public static function verifyIsinString($input): void
    {
        self::verifyNonEmptyString($input);
        if (1 === Preg::match(Isin::ISIN_REGEX, $input)) {
            return;
        }

        throw new InvalidArgumentException('Input is not a valid ISIN string!');
    }
}