<?php
declare(strict_types = 1);

namespace util\exception;

use Exception;
use Throwable;

class PregException extends Exception
{
    private const ERROR_MAPPING = [
        PREG_INTERNAL_ERROR => 'Internal Error',
        PREG_BACKTRACK_LIMIT_ERROR => 'Backtrack Limit Error',
        PREG_RECURSION_LIMIT_ERROR => 'Recursion Limit Error',
        PREG_BAD_UTF8_ERROR => 'Bad UTF-8 ERROR',
        PREG_BAD_UTF8_OFFSET_ERROR => 'Bad UTF-8 Offset Error',
        PREG_JIT_STACKLIMIT_ERROR => 'JIT Stack limit Error',
    ];

    public function __construct(string $message = '', int $code = 1, Throwable $previous = null)
    {
        $message = '' === $message
            ? self::ERROR_MAPPING[$code]
            : $message . ' (' . self::ERROR_MAPPING[$code] . ')';

        parent::__construct($message, $code, $previous);
    }
}
