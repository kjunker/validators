<?php
declare(strict_types = 1);

namespace model;

use InvalidArgumentException;
use util\exception\PregException;
use util\Preg;
use util\Verifier;

class Isin
{
    public const ISIN_REGEX = '/^(?<ISO31662>[A-Z]{2})'
        . '(?<alphanum>[A-Z0-9]{9})'
        . '(?<checkdigit>[0-9]{1})?$/';

    /**
     * @var string
     */
    private $alphanum;
    /**
     * @var int|null
     */
    private $checkdigit;

    /**
     * @var string
     */
    private $isin;
    /**
     * @var string
     */
    private $iso3166_2;

    private function __construct(string $iso3166_2, string $alphanum, ?int $checkdigit)
    {
        if ('' === $iso3166_2) {
            throw new InvalidArgumentException('ISO 3166 2 Param should not be an empty string!');
        }
        if ('' === $alphanum) {
            throw new InvalidArgumentException('alphanum should be an non empty string!');
        }
        if (9 !== strlen($alphanum)) {
            throw new InvalidArgumentException('Alphanum should be an nine character long string!');
        }
        if (null !== $checkdigit && 0 > $checkdigit) {
            throw new InvalidArgumentException('Check digit should be NULL or a non negative Integer!');
        }
        $this->isin = $iso3166_2 . $alphanum . $checkdigit;
        $this->iso3166_2 = $iso3166_2;
        $this->alphanum = $alphanum;
        $this->checkdigit = $checkdigit;
    }

    /**
     * Creates a new Isin Object with or without the checkdit
     * Input must be 11 or 12 chars long with a two letters prefix
     * follows by a nine chars long alhpanumeric string
     * and can be including the checkdigit
     *
     * @param string $input
     * @return Isin
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function createFromString(string $input): self
    {
        Verifier::verifyNonEmptyString($input);
        if (0 === Preg::match(self::ISIN_REGEX, $input, $matches)) {
            throw new InvalidArgumentException('Input is not a valid ISIN string!');
        }
        $checkdigit = (null === ($checkdigit = $matches['checkdigit'] ?? null))
            ? $checkdigit
            : (int)$checkdigit;

        return new self($matches['ISO31662'], $matches['alphanum'], $checkdigit);
    }

    /**
     * @return string
     */
    public function getAlphanum(): string
    {
        return $this->alphanum;
    }

    /**
     * @return int|null
     */
    public function getCheckdigit(): ?int
    {
        return $this->checkdigit;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @return string
     */
    public function getIso31662(): string
    {
        return $this->iso3166_2;
    }
}